module.exports = {
  title: 'Руководство: SRM-Портал поставщиков',
  themeConfig: {
      nav: [
        { text: 'Главная', link: '/' },
        { text: 'Документация', link: '/guide/' },
      ],
      sidebar: {
          '/guide/': [
              '',
              'anketa',
			  'work',
			  'start',
			  'card',			  
			  'chat',
			  'menu',
			  'profiles',			  
			  'audit',
			  'procedure',
			  'orders',
			  'quality',
			  'logistics',
          ],
      }
  },
  base:"/dist/"
}