# Доступ поставщика на портал

Вход в систему для уже зарегистрированного поставщика осуществляется по логину и паролю.  
Для регистрации на портале необходимо ознакомиться с правилами использования портала и положениями политики конфиденциальности и пройти регистрацию. 

<img :src="$withBase('/img/image001.png')">
Рис. Информация для новых поставщиков на портале.

Регистрация поставщика на портале осуществляется по результатам <i style="color:#319fff"><u>первичного анкетирования</u></i>. По результатам проверки первичной анкеты, в случае положительного решения об аккредитации поставщика, он регистрируется в системе PROF-IT SRM, создается карточка и личный кабинет контрагента, для каждого из указанных контактных лиц поставщика генерируется свой логин и пароль, которые отправляются на указанную в анкете электронную почту (рассылка идет на почту указанных в анкете контактных лиц). 
Предусмотрены следующие роли для контактных лиц поставщика на портале: 

SRM Администратор портала (доступны для просмотра и редактирования все документы на портале)
SRM Пользователь портала (доступны для просмотра все документы на портале, недоступны для редактирования анкеты)
SRM Пользователь портала логист (доступны для просмотра и редактирования только документы, связанные с логистикой) 
Роль SRM Администратор портала может быть присвоена только одну пользователю каждого контрагента. Ограничений по количеству пользователей с ролями SRM Пользователь портала и SRM Пользователь портала логист нет. 
