import Vue from 'vue';
const moment = require('moment');
Vue.filter('uncamelize', str => {
	if (str === "КооперацияПоГОЗ"){
		return 'Кооперация по ГОЗ';
	}
	if (str === "БезНДС"){
		return 'Без НДС';
	}
	if (str === "НДС0"){
		return '0%';
	}
	if (str === "НДС10"){
		return '10%';
	}
	if (str === "НДС20"){
		return '20%';
	}
	if (str === "ДоговорNDA"){
		return 'Договор NDA';
	}
	if (str === "ВвозИзЕАЭС"){
		return 'Ввоз из ЕАЭС';
	}
	if (str === "наЭТП"){
		return 'На ЭТП';
	}	
	if (str === "НаПодписанииУВП"){
		return 'На подписании у ВП';
	}
	if (str === "СогласованоСВП"){
		return 'Согласовано с ВП';
	}
	if (str === "ОтказаноВП"){
		return 'Отказано ВП';
	}
	if (str === "НаСогласованииПоЕПоЗ39"){
		return 'На согласовании по ЕПоЗ 39';
	}
	if (str === "СертификатСМК"){
		return 'Сертификат СМК';
	}
	if (str === "ВыпискаИзЕГРЮЛ"){
		return 'Выписка из ЕГРЮЛ';
	}
	if (str === "ВыпискаИзЕГРИП"){
		return 'Выписка из ЕГРИП';
	}
	if (str === "ВедетсяАудит"){
		return 'Проведение аудита';
	}
	if (str === "СформированаОценкаПоставщика"){
		return 'Аудит завершен';
	}
	if (str === "АудитЗавершен"){
		return 'Оценка завершена';
	}
	if (str === "ДоставкаТМЦНаПредприятие"){
		return 'Доставка ТМЦ на предприятие';
	}
	if (str === "ОтгрузкаТМЦСПредприятия"){
		return 'Отгрузка ТМЦ с предприятия';
	}
	if (str === "АудитДействующегоПоставщика"){
		return 'Оценка действующего поставщика';
	}	
	let firstLetter = str.charAt(0).toUpperCase();
	str = firstLetter + str.replace(/([A-ZА-Я])/g,' $1').substr(2).toLowerCase();
	return str;
});
Vue.filter('formateDate', (date,dateFormat) => {
	if(!date){
		return '';
	}
	if(date == '0001-01-01T00:00:00') {
		return '';
	}
	if(date === "бессрочно"){
		return "бессрочно"
	}
	if(!dateFormat){ 
		return moment(date).format('DD.MM.YYYY');
	}
	return moment(date).format(dateFormat);
});

export function colorStatus(cssStatus) {
	if(cssStatus === "Черновик" || cssStatus === "ОтказВПоставке" || cssStatus === "ПредложениеНеПодано"){
		return 'status yellow';
	}
	if(cssStatus === "Направлена на рассмотрение" || cssStatus === "НаправленНаРассмотрение" || cssStatus === "НаправленНаСогласование" || cssStatus === "На согласовании" || cssStatus === "ДопущенКУчастиюВСледующемЭтапеЗакупочнойПроцедуры" || cssStatus === "НаСогласованииУПоставщика" || cssStatus === "СогласованиеШагаD0" || cssStatus === "СогласованиеШагаD3" || cssStatus === "СогласованиеШагаD5" || cssStatus === "МероприятияПроизведеныПоставщиком" || cssStatus === "ПодтвержденаЛогистическойСлужбой" || cssStatus === "КОтгрузке" || cssStatus === "НаСогласованииУПеревозчика" || cssStatus === "Выполняется" || cssStatus === "НаправленаПоставщику" || cssStatus === "НаправленоПоставщику" || cssStatus === "НаСогласованииВКомпании" || cssStatus === "НаСогласованииУПоставщика" || cssStatus === "ПроведениеАудита" || cssStatus === "СогласованиеСкорректированныхДат" || cssStatus === "НаправленНаРассмотрениеВПервомЭтапеЗакупочнойПроцедуры" || cssStatus === "НаправленоПоставщику"){
		return 'status blue';
	}	
	if(cssStatus === "Согласованo" || cssStatus === "Согласована" || cssStatus === "Согласован" || cssStatus === "ПринятаПеревозчиком" || cssStatus === "ДопущенКУчастию" || cssStatus === "Подтвержден" || cssStatus === "СогласованШагD0" || cssStatus === "СогласованШагD3" || cssStatus === "СогласованШагD5" || cssStatus === "Исполнена" || cssStatus === "Доставлено" || cssStatus === "СогласованаПоставщиком" || cssStatus === "СогласованоПоставщиком" || cssStatus === "АудитЗавершен" || cssStatus === "ДатаСогласованаПоставщиком" || cssStatus === "Утверждена" || cssStatus === "Утверждено" || cssStatus === "Закрыто" || cssStatus === "СогласованаВКомпании"){
		return 'status green';
	}	
	if(cssStatus === "Не согласованo" || cssStatus === "Не согласована" || cssStatus === "НеДопущенКУчастию" || cssStatus === "ОтклонёнШагD0" || cssStatus === "ОтклонёнШагD3" || cssStatus === "ОтклонёнШагD5" || cssStatus === "ОтчётЗакрыт" || cssStatus === "ОтчётНеСогласованЗаказчиком" || cssStatus === "Отменена" || cssStatus === "НеСогласованПеревозчиком" || cssStatus === "Не согласована" || cssStatus === "ОтклоненаПоставщиком" || cssStatus === "ОтклоненоПоставщиком" || cssStatus === "ОтклоненаВКомпании" || cssStatus === "НеСогласованаПеревозчиком" || cssStatus === "НеПодтвержденПоставщиком"  || cssStatus === "НеПодтвержденПоставщиком" || cssStatus === "ОтклоненПоставщиком" || cssStatus === "НеСогласован"  || cssStatus === "Закрыт"){
		return 'status red';
	}	
	return 'status';
};

export function colorCardStatus(cssStatus) {
	/*card->Анкета поставщика*/
	if(cssStatus === "Согласована" || cssStatus === "СогласованаПоставщиком"){
		return 'card_color_status green_card card_width_anketa';
	}		
	if(cssStatus === "Не согласована" || cssStatus === "ОтклоненаПоставщиком"){
		return 'card_color_status red_card card_width_anketa';
	}
	if(cssStatus === "Направлена на рассмотрение"|| cssStatus === "На согласовании" || cssStatus === "НаправленаПоставщику"  || cssStatus === "НаправленоПоставщику"){
		return 'card_color_status blue_card card_width_anketa';
	}
	/*card->Товарные категории*/
	if(cssStatus === "Аккредитован"){
		return 'card_color_status green_card card_width';
	}		
	if(cssStatus === "Лишен аккредитации"){
		return 'card_color_status red_card card_width';
	}
	/*Оценка поставщика - не нужно
	if(cssStatus === 1 || cssStatus === 2){
		return 'card_color_status red_card card_width_rating';
	}	
	if(cssStatus === 3){
		return 'card_color_status yellow_card card_width_rating';
	}		
	if(cssStatus === 4 || cssStatus === 5){
		return 'card_color_status green_card card_width_rating';		
	}	*/
	return 'card_color_status';
}