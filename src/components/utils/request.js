import Vue from 'vue';
const axios = require('axios');
export function request(url,data){
	axios({
		method: 'post',
        url: url,
        data: data
	}).then(
		(response) => {
            if(response.data.status == "error"){
                this.$vs.notify({title:'Ошибка',text:response.data.result,color:'danger', fixed:true})
                return false;
            }
			else{
                return response.data;
            }
	});
}
export function checkRequest(resp){
    if(resp.data.status == "error"){
        return resp.data.result;
    }
    else{
        return false;
    }
}