import Vue from 'vue';
Vue.filter('numberBeautifier', (x) => {
	  var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
});
Vue.filter('removeZeroes', (str) => {
    return str.replace(/^0+/, '');
});