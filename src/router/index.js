import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Login from '@/components/pages/Login'
import Anketa from '@/components/pages/Anketa'
import Anketa_table from '@/components/tables/Anketa_table'
import Supplier_offer_table from '@/components/tables/Supplier_offer_table'
import Supplier_offer_card from '@/components/pages/Supplier_offer_card'
import Anketa_profile from '@/components/pages/Anketa_profile'
import Card from '@/components/pages/Card'
import News from '@/components/pages/News'
import News_list from '@/components/pages/News_list'
import Partner_contract from '@/components/pages/Partner_contract'
import Request_supplier_table from '@/components/tables/Request_supplier_table'
import Request_supplier_card from '@/components/pages/Request_supplier_card'
import Request_supplier_card_profile from '@/components/pages/Request_supplier_card_profile'
import Audit_program_table from '@/components/tables/Audit_program_table'
import Audit_program_card from '@/components/pages/Audit_program_card'
import Self_assessment_sheet from '@/components/pages/Self_assessment_sheet'
import Order_supplier_card from '@/components/pages/Order_supplier_card'
import Order_supplier_inform from '@/components/pages/Order_supplier_inform'
import Order_supplier_table from '@/components/tables/Order_supplier_table'
import Proposal_audit_application_table from '@/components/tables/Proposal_audit_application_table'
import Proposal_audit_application_card from '@/components/pages/Proposal_audit_application_card'
import Pretense_table from '@/components/tables/Pretense_table'
import Pretense_card from '@/components/pages/Pretense_card'
import Rating_supplier_table from '@/components/tables/Rating_supplier_table'
import Rating_supplier_card from '@/components/pages/Rating_supplier_card'
import Action_plan_table from '@/components/tables/Action_plan_table'
import Action_plan_card from '@/components/pages/Action_plan_card'
import Application_delivery_table from '@/components/tables/Application_delivery_table'
import Application_delivery_card from '@/components/pages/Application_delivery_card'
import Application_transport_table from '@/components/tables/Application_transport_table'
import Application_transport_card from '@/components/pages/Application_transport_card'
import Report_table from '@/components/report/Report_table'
import Report_card from '@/components/report/Report_card'
import Report_diagram from '@/components/report/Report_diagram'
import Report_column from '@/components/report/Report_column'
import Report_column2 from '@/components/report/Report_column2'
import Report_angular from '@/components/report/Report_angular'
import Report_new from '@/components/report/Report_new'
import Defect_report from '@/components/report/Defect_report'
import ReportG8D from '@/components/report/ReportG8D'
import Report_order_analysis from '@/components/report/Report_order_analysis'
import Messenges from '@/components/pages/Messenges'
import Supplier_files from '@/components/pages/Supplier_files'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/anketa',
      name: 'Anketa',
      component: Anketa
    },
    {
      path: '/anketa_table',
      name: 'Anketa_table',
      component: Anketa_table
    },
    {
      path: '/supplier_offer_table',
      name: 'Supplier_offer_table',
      component: Supplier_offer_table
    },
    {
      path: '/supplier_offer_card',
      name: 'Supplier_offer_card',
      component: Supplier_offer_card
    },
    {
      path: '/anketa_profile',
      name: 'Anketa_profile',
      component: Anketa_profile
    },
    {
      path: '/card',
      name: 'Card',
      component: Card
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/news_list',
      name: 'News_list',
      component: News_list
    },
    {
      path: '/partner_contract',
      name: 'Partner_contract',
      component: Partner_contract
    },
    {
      path: '/request_supplier_table',
      name: 'Request_supplier_table',
      component: Request_supplier_table
    },
    {
      path: '/request_supplier_card',
      name: 'Request_supplier_card',
      component: Request_supplier_card
    },
    {
      path: '/request_supplier_card_profile',
      name: 'Request_supplier_card_profile',
      component: Request_supplier_card_profile
    },
    {
      path: '/audit_program_table',
      name: 'Audit_program_table',
      component: Audit_program_table
    },
    {
      path: '/audit_program_card',
      name: 'Audit_program_card',
      component: Audit_program_card
    },
    {
      path: '/self_assessment_sheet',
      name: 'Self_assessment_sheet',
      component: Self_assessment_sheet
    },	
    {
      path: '/order_supplier_card',
      name: 'Order_supplier_card',
      component: Order_supplier_card
    },
    {
      path: '/order_supplier_inform',
      name: 'Order_supplier_inform',
      component: Order_supplier_inform
    },
    {
      path: '/order_supplier_table',
      name: 'Order_supplier_table',
      component: Order_supplier_table
    },
    {
      path: '/proposal_audit_application_table',
      name: 'Proposal_audit_application_table',
      component: Proposal_audit_application_table
    },
    {
      path: '/proposal_audit_application_card',
      name: 'Proposal_audit_application_card',
      component: Proposal_audit_application_card
    },
    {
      path: '/pretense_table',
      name: 'Pretense_table',
      component: Pretense_table
    },
    {
      path: '/pretense_card',
      name: 'Pretense_card',
      component: Pretense_card
    },
    {
      path: '/rating_supplier_table',
      name: 'Rating_supplier_table',
      component: Rating_supplier_table
    },
    {
      path: '/rating_supplier_card',
      name: 'Rating_supplier_card',
      component: Rating_supplier_card
    },
    {
      path: '/action_plan_table',
      name: 'Action_plan_table',
      component: Action_plan_table
    },
    {
      path: '/action_plan_card',
      name: 'Action_plan_card',
      component: Action_plan_card
    },
    {
      path: '/application_delivery_table',
      name: 'Application_delivery_table',
      component: Application_delivery_table
    },
    {
      path: '/application_delivery_card',
      name: 'Application_delivery_card',
      component: Application_delivery_card
    },	
    {
      path: '/application_transport_table',
      name: 'Application_transport_table',
      component: Application_transport_table
    },	
    {
      path: '/application_transport_card',
      name: 'Application_transport_card',
      component: Application_transport_card
    },	
    {
      path: '/report_table',
      name: 'Report_table',
      component: Report_table
    },	
    {
      path: '/report_card',
      name: 'Report_card',
      component: Report_card
    },
    {
      path: '/report_diagram',
      name: 'Report_diagram',
      component: Report_diagram
    },
    {
      path: '/report_angular',
      name: 'Report_angular',
      component: Report_angular
    },
    {
      path: '/report_column',
      name: 'Report_column',
      component: Report_column
    },
    {
      path: '/report_column2',
      name: 'Report_column2',
      component: Report_column2
    },
    {
      path: '/report_new',
      name: 'Report_new',
      component: Report_new
    },
    {
      path: '/defect_report',
      name: 'Defect_report',
      component: Defect_report
    },
    {
      path: '/ReportG8D',
      name: 'ReportG8D',
      component: ReportG8D
    },
    {
      path: '/Report_order_analysis',
      name: 'Report_order_analysis',
      component: Report_order_analysis
    },
    {
      path: '/messenges',
      name: 'Messenges',
      component: Messenges
    },
    {
      path: '/supplier_files',
      name: 'Supplier_files',
      component: Supplier_files
    },
  ]
})
