﻿const PAGE_NAME = {
			PAGE_NAME1: "Сообщения",                     	//Заголовок - Сообщения
			TEMA: "Добавить тему",                      	//Всплывающая подсказка - Добавить тему
			ACCEPT: "Отправить",                         	//кнопка в попап - Отправить
			CANCEL: "Отмена",                          	 	//кнопка в попап - отмена
			TITLE: "Добавить чат",              	 		//кнопка в попап - Добавить чат
			MESS1: "Сообщение менеджеру",                   //попап - Сообщение менеджеру
			MESS2: "Сообщение по теме",                     //попап - Сообщение по теме
		};
export default {
	PAGE_NAME: PAGE_NAME,
}