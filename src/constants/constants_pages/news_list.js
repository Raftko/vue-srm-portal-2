﻿const PAGE_NAME = {
			RETURN: "Вернуться к списку", 		//Всплывающая подсказка - Вернуться к списку
			PAGE_NAME1: "Лента новостей",       //Заголовок - Лента новостей
			IMPORTANT1: "Очень важно!",       	//Категория 1 - Очень важно!
			IMPORTANT2: "Важно!",       		//Категория 2 - Важно!
		};
export default {
	PAGE_NAME: PAGE_NAME,
}