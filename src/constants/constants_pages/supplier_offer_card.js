﻿const PAGE_NAME = {
			RETURN: "Вернуться к списку", 				//Всплывающая подсказка - Вернуться к списку
			PAGE_NAME_H: "Запрос предложений: № ", 		//Заголовок - Запрос предложений: № 
			PAGE_NAME1: "Основное",                     //Таб 1 - Основное
			PAGE_NAME2: "Товары",                  		//Таб 2 - Товары
			PAGE_NAME3: "Требования к поставщикам",     //Таб 3 - Требования к поставщикам
			PAGE_NAME4: "Требования к номенклатуре",    //Таб 4 - Требования к номенклатуре
			PAGE_NAME5: "Условия поставки",             //Таб 5 - Условия поставки
			PAGE_NAME6: "Переторжки",         			//Таб 6 - Переторжки
			PAGE_NAME7: "Присоединенные файлы",         //Таб 7 - Присоединенные файлы
			//Таб 1 - Основное
			SUBJECT_PURCHASE: "Предмет закупки",         //Предмет закупки
			//Таб 2 - Товары	
			COLUMNS: {
				COLUMN1: "№",                  				//Заголовок столбца 1 (в Таблице) - №
				COLUMN2: "Артикул",                   		//Заголовок столбца 2 (в Таблице) - Артикул
				COLUMN3: "Наименование",                	//Заголовок столбца 3 (в Таблице) - Наименование
				COLUMN4: "НМЦ",                  			//Заголовок столбца 4 (в Таблице) - НМЦ
				COLUMN5: "Количество",                   	//Заголовок столбца 5 (в Таблице) - Количество
				COLUMN6: "Ед.изм.",                			//Заголовок столбца 6 (в Таблице) - Ед.изм.
				COLUMN7: "Ставка НДС",                  	//Заголовок столбца 7 (в Таблице) - Ставка НДС
				COLUMN8: "Стоимость с НДС",                 //Заголовок столбца 8 (в Таблице) - Стоимость с НДС
				COLUMN9: "Сумма НДС",                  		//Заголовок столбца 9 (в Таблице) - Сумма НДС
				COLUMN10: "Цена с НДС",                  	//Заголовок столбца 10 (в Таблице) - Цена с НДС		
				COLUMN11: "Период планирования",            //Заголовок столбца 11 (в Таблице) - Период планирования
				COLUMN12: "Количество",                  	//Заголовок столбца 12 (в Таблице) - Количество			
			},
			//Таб 3 - Требования к поставщикам & Таб 4 - Требования к номенклатуре
			REQ_SUP: {
				REQ_SUP1: "№",                  			//Заголовок столбца 1 (в Таблице) - №
				REQ_SUP2: "Требование",                   	//Заголовок столбца 2 (в Таблице) - Требование
				REQ_SUP3: "Критерий, выражающий требование",//Заголовок столбца 3 (в Таблице) - Критерий, выражающий требование
			},
			//Таб 5 - Условия поставки
			COMMENT: "Комментарий",                    		//Комментарий
			//Таб 6 - Переторжки
			PERETOR: {
				PERETOR1: "Номенклатура",                  			//Заголовок столбца 1 (в Таблице) - Номенклатура
				PERETOR2: "Единица измерения",                   	//Заголовок столбца 2 (в Таблице) - Единица измерения
				PERETOR3: "Количество итого",						//Заголовок столбца 3 (в Таблице) - Количество итого
				PERETOR4: "Целевая цена без НДС",               	//Заголовок столбца 4 (в Таблице) - Целевая цена без НДС
				PERETOR5: "Валюта запроса",                   		//Заголовок столбца 5 (в Таблице) - Валюта запроса
				REBIDDING: "Создать предложение для переторжки",	//кнопка - Создать предложение для переторжки
			},			
			//Таб 7 - Присоединенные файлы	
			FILE: {
				FILE1: "Наименование",                      //Заголовок столбца 1 (в Таблице) - Наименование
				FILE2: "Вид документа",                     //Заголовок столбца 2 (в Таблице) - Вид документа
				FILE3: "Дата создания",                     //Заголовок столбца 3 (в Таблице) - Дата создания
			},
		};
export default {
	PAGE_NAME: PAGE_NAME,
}